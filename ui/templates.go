package ui

import (
	"fmt"
	"gitlab.com/T4cC0re/muzzleflash/types"
	timetrack "gitlab.com/T4cC0re/time-track"
	"html/template"
	"io"
	"net/http"
	"path"
	"time"
)

type Template struct {
	template *template.Template
}

func NewTemplate(basedir string) (tpl *Template, err error) {
	tpl = &Template{}
	tpl.template, err = template.ParseGlob(path.Join(basedir, "html/*"))
	return
}

func (t *Template) Render(tplId string, w io.Writer, data interface{}) error {
	defer timetrack.TimeTrack(time.Now())
	return t.template.ExecuteTemplate(w, tplId, data)
}

func (t *Template) Error(statusCode int, w io.Writer) error {
	defer timetrack.TimeTrack(time.Now())
	text := http.StatusText(statusCode)
	if text == "" {
		text = fmt.Sprintf("Unknown Error (%d)", statusCode)
	}

	data := types.DialogData{ErrorText: text}

	return t.Render("error", w, data)
}
