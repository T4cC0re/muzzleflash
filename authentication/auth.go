package authentication

import (
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"gitlab.com/T4cC0re/muzzleflash/types"
	"gitlab.com/T4cC0re/muzzleflash/ui"
	"gitlab.com/T4cC0re/muzzleflash/util"
	"gitlab.com/T4cC0re/time-track"
	"log"
	"net/http"
	"net/url"
	"strings"
	"time"
)

const LOGIN_PATH = "/-/muzzleflash/auth/"
const COOKIE_NAME = "muzzleflash_auth"
const QUERY_NAME = "muzzleflash_auth"

const EXPIRY_TIME = time.Hour * 72
const EXPIRY_TIME_SHORT_LIVED = time.Second * 30
const EXPIRY_REGEN_TIME = time.Minute * 5

var uiTemplate *ui.Template
var plainCreds map[string]string
var jwtSecret = util.GenerateRandomBytes(16)

func init() {
	plainCreds = map[string]string{}
}

func SetJWTSecret(secret []byte) {
	jwtSecret = secret
}

/**
IsAuthenticated
Returns
bool Request is authenticated
bool A redirect has been served to strip a token
*/
func IsAuthenticated(req *types.Request) (bool, bool) {
	defer timetrack.TimeTrack(time.Now())
	if queryTokenToCookie(req) {
		return true, true
	}

	token := extractToken(req)

	log.Printf("token: %s", token)

	if sub := checkAndRenewTokenIfRequired(token, req); sub != "" {
		fmt.Println("token:", token)
		req.SetHeader("X-Muzzleflash-Auth", token)
		req.SetHeader("X-Muzzleflash-User", sub)
		return true, false
	}
	log.Printf("token '%s' apparently invalid", token)

	return false, false
}

func setCookieIfToken(token string, request *types.Request) {
	if token == "" {
		return
	}

	setCookie(token, request)
}

func setCookie(token string, request *types.Request) {
	cookie := http.Cookie{
		Name:     COOKIE_NAME,
		Value:    token,
		SameSite: http.SameSiteLaxMode,
		Secure:   true,
		Path:     "/",
		HttpOnly: true,
	}
	request.SetHeader("Set-Cookie", cookie.String())
}

func queryTokenToCookie(req *types.Request) bool {
	defer timetrack.TimeTrack(time.Now())
	log.Printf("queryTokenToCookie")
	q := req.OrigRequest.URL.Query()
	token := q.Get(QUERY_NAME)

	sub := checkAndRenewTokenIfRequired(token, req)
	switch {
	case sub != "":
		req.SetHeader("X-Muzzleflash-Auth", sub)
		req.SetHeader("X-Muzzleflash-User", sub)
		fallthrough
	case token != "":
		// Even if the token is not valid: if there is one, remove it
		q.Del(QUERY_NAME)
		req.OrigRequest.URL.RawQuery = q.Encode()
		req.Redirect(req.OrigRequest.URL.String(), http.StatusTemporaryRedirect)
		return true
	}
	return false
}

func extractToken(req *types.Request) string {
	defer timetrack.TimeTrack(time.Now())
	log.Printf("extractToken")
	cookie, _ := req.OrigRequest.Cookie(COOKIE_NAME)
	header := req.OrigRequest.Header.Get("X-Muzzleflash-Auth")

	token := ""
	if cookie != nil && cookie.Value != "" {
		token = cookie.Value
	}
	if header != "" {
		token = header
	}

	return token
}

func isValidLogin(username string, password string) bool {
	defer timetrack.TimeTrack(time.Now())

	log.Printf("isValidLogin")
	// TODO: More than just stupid plaintext passwords
	if pass := plainCreds[username]; pass != "" && pass == password {
		return true
	}
	return false
}

func checkAndRenewTokenIfRequired(tokenToCheck string, req *types.Request) string {
	defer timetrack.TimeTrack(time.Now())

	log.Printf("checkAndRenewTokenIfRequired")
	if tokenToCheck == "" {
		return ""
	}
	token, err := jwt.Parse(tokenToCheck, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("unexpected signing method: %v", token.Header["alg"])
		}

		return jwtSecret, nil
	})

	if token == nil || err != nil {
		fmt.Println("token invalid", err)
		return ""
	}

	var claims jwt.MapClaims
	var ok bool
	if claims, ok = token.Claims.(jwt.MapClaims); ok && token.Valid {
	} else {
		fmt.Println("token invalid", claims)
		return ""
	}

	sub, _ := claims["sub"].(string)
	aud, _ := claims["aud"].(string)

	// A token will always have the domain in the audience field
	if aud == "" {
		return ""
	}

	if strings.EqualFold(req.Hostname, aud) {
		// Determine if the token needs to be renewed
		if !claims.VerifyExpiresAt(time.Now().Add(EXPIRY_REGEN_TIME).Unix(), true) {
			// Token is invalid in 5 minutes, take the sub and aud from the validated token and resign
			// An error while generating the token is unlikely and should be ignored, as this is a transparent request
			newToken, _ := createToken(sub, aud, EXPIRY_TIME)
			setCookieIfToken(newToken, req)
			return sub
		}

		return sub
	}

	return ""
}

func issueTokenAndRedirect(req *types.Request, r *http.Request, subject string, redirect string) {
	defer timetrack.TimeTrack(time.Now())

	log.Printf("issueTokenAndRedirect")
	u, err := url.Parse(redirect)
	if err != nil {
		ShowLogin(req, u.String(), 500, err.Error(), subject)
	}

	var token string

	// If we are on the same domain the cookie is enough.
	// No need to do the redirectToken-bounce-dance
	// Alias 'if query-token'
	if u.Host != req.VHost.Host {
		// Create a short-lived token. This only needs to survive a few seconds as the endpoint just serves the redirect.
		redirectToken, err := createToken(subject, u.Host, EXPIRY_TIME_SHORT_LIVED)
		if err != nil {
			ShowLogin(req, u.String(), 500, err.Error(), subject)
			return
		}

		// Append the short-lived token as a query, that is to be converted to a long-lived token upon access
		q := u.Query()
		q.Set(QUERY_NAME, redirectToken)
		u.RawQuery = q.Encode()
	}
	// Set Cookie with a token for the current host.
	// If we are on the login page, this will allow the user to log into other services on this host easily.
	// If not, set a cookie anyways, as we are on the target vHost :)
	token, err = createToken(subject, req.VHost.Host, EXPIRY_TIME)
	if err != nil {
		ShowLogin(req, u.String(), 500, err.Error(), subject)
		return
	}
	setCookieIfToken(token, req)

	req.SetHeader("X-Muzzleflash-Auth", token)
	req.SetHeader("X-Muzzleflash-User", subject)
	req.Redirect(u.String(), 303)
	return
}

func createToken(subject, audience string, validity time.Duration) (string, error) {
	defer timetrack.TimeTrack(time.Now())
	tokenObject := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
		"sub": subject,
		"nbf": time.Now().Add(time.Second * -30).Unix(),
		"exp": time.Now().Add(validity).Unix(),
		"aud": audience,
	})

	token, err := tokenObject.SignedString(jwtSecret)
	if err != nil {
		return "", err
	}

	return token, nil
}

func BuildLink(loginHost string, confirmLogin bool, origRequest *http.Request) string {
	defer timetrack.TimeTrack(time.Now())

	log.Printf("BuildLink")
	endpoint := "goto"
	if confirmLogin {
		endpoint = "confirm"
	}
	return fmt.Sprintf("https://%s%s%s/%s%s", loginHost, LOGIN_PATH, endpoint, origRequest.Host, origRequest.RequestURI)
}

func handleGETRequest(req *types.Request, redirect string, tokenSubject string, mustConfirm bool) {
	defer timetrack.TimeTrack(time.Now())
	if tokenSubject == "" {
		ShowLogin(req, redirect, 200, "", "")
		return
	}
	if redirect == "" {
		ShowControlPanel(req, tokenSubject)
	} else if mustConfirm {
		ShowConfirmationDialog(req, redirect, tokenSubject)
	} else {
		issueTokenAndRedirect(req, req.OrigRequest, tokenSubject, redirect)
	}
}

func HandleFrontend(req *types.Request) {
	var redirect string
	var mustConfirm bool
	var isLogout bool
	var tokenSubject string

	defer timetrack.TimeTrack(time.Now())

	_ = queryTokenToCookie(req)

	token := extractToken(req)
	tokenSubject = checkAndRenewTokenIfRequired(token, req)
	mustConfirm, redirect, isLogout = evaluateURL(req, req.OrigRequest.RequestURI)

	if req.OrigRequest.Method == "POST" {
		handleLoginFormPOST(req, redirect)
		return
	} else {
		if isLogout {
			handleLogout(req)
			return
		}
		handleGETRequest(req, redirect, tokenSubject, mustConfirm)
		return
	}

}

func handleLogout(req *types.Request) {
	defer timetrack.TimeTrack(time.Now())
	setCookie("", req) // Deletes the cookie
	ShowLogin(req, "", 302, "", "")
}

func evaluateURL(req *types.Request, url string) (mustConfirm bool, redirect string, isLogout bool) {
	defer timetrack.TimeTrack(time.Now())
	if len(url) > len(LOGIN_PATH) {
		parts := strings.SplitN(url[len(LOGIN_PATH):], "/", 3)
		if len(parts) >= 2 {
			redirect = fmt.Sprintf("https://%s", strings.Join(parts[1:], "/"))
		}
		switch parts[0] {
		case "confirm":
			mustConfirm = true
		case "logout":
			isLogout = true
		case "goto":
			if parts[1] != req.VHost.LoginHost && req.VHost.PureLogin {
				break
			}
			log.Println("Don't redirect if this is a pure login host and the target is on this host")
			fallthrough
		default:
			redirect = ""
		}
	}
	return
}

func handleLoginFormPOST(req *types.Request, redirect string) {
	defer timetrack.TimeTrack(time.Now())
	_ = req.OrigRequest.ParseForm()
	username := strings.Join(req.OrigRequest.Form["username"], "")
	password := strings.Join(req.OrigRequest.Form["password"], "")
	redirect = strings.Join(req.OrigRequest.Form["redirect"], "")

	fmt.Println("username:", username)
	fmt.Println("password:", len(password))
	fmt.Println("redirect:", req.OrigRequest.Form["redirect"])

	var sub string

	if isValidLogin(username, password) {
		req.Authenticated = true
		sub = username
	}

	if req.Authenticated == true {
		issueTokenAndRedirect(req, req.OrigRequest, sub, redirect)
		return
	} else {
		ShowLogin(req, redirect, 401, "Invalid credentials!", username)
		return
	}
}

func ShowLogin(req *types.Request, redirect string, code int, errorText string, attemptedUsername string) {
	defer timetrack.TimeTrack(time.Now())

	req.SetHeader("content-type", "text/html")
	req.WriteHeader(code)

	dialogData := types.DialogData{
		Error:     errorText != "",
		ErrorText: errorText,
		Hostname:  req.Hostname,
		Redirect:  redirect,
		Username:  attemptedUsername,
	}

	err := uiTemplate.Render("login", *req.ResponseWriter, dialogData)
	log.Println(err)
}

func ShowConfirmationDialog(req *types.Request, redirect string, tokenSubject string) {
	defer timetrack.TimeTrack(time.Now())

	req.SetHeader("content-type", "text/html")
	req.WriteHeader(401)

	dialogData := types.DialogData{
		Redirect: redirect,
		Username: tokenSubject,
		Hostname: req.Hostname,
	}

	err := uiTemplate.Render("confirm", *req.ResponseWriter, dialogData)
	log.Println(err)
}

func ShowControlPanel(req *types.Request, tokenSubject string) {
	defer timetrack.TimeTrack(time.Now())

	req.SetHeader("content-type", "text/html")
	req.WriteHeader(200)

	dialogData := types.DialogData{
		Username: tokenSubject,
		Hostname: req.Hostname,
	}

	err := uiTemplate.Render("control", *req.ResponseWriter, dialogData)
	log.Println(err)
}

func ParseTemplates(basedir string) {
	var err error
	uiTemplate, err = ui.NewTemplate(basedir)
	if err != nil {
		log.Fatal(err)
	}
}

func AddPlainCredential(username string, password string) {
	defer timetrack.TimeTrack(time.Now())
	log.Printf(
		"Adding plain-text credential for username '%s'",
		username,
	)
	plainCreds[username] = password
}
