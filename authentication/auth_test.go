package authentication

import "testing"

func TestIsValidLogin(t *testing.T) {
	username1 := "testuser1"
	username2 := "testuser2"
	password1 := "testpassword1"
	password2 := "testpassword2"

	AddPlainCredential(username1, password1)
	AddPlainCredential(username2, password2)

	if !isValidLogin(username1, password1) {
		t.Errorf("Expected username1 and password1 to return true")
	}
	if !isValidLogin(username2, password2) {
		t.Errorf("Expected username2 and password2 to return true")
	}
	if isValidLogin(username1, "") {
		t.Errorf("Expected username1 without password to return false")
	}
	if isValidLogin(username1, password2) {
		t.Errorf("Expected username1 and password2 to return false")
	}
	if isValidLogin("", password2) {
		t.Errorf("Expected no username with passwotd to return false")
	}
	if isValidLogin("", "") {
		t.Errorf("Expected no username nor password to return false")
	}
}
