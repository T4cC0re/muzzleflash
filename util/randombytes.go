package util

import (
	"crypto/rand"
	timetrack "gitlab.com/T4cC0re/time-track"
	"time"
)

func GenerateRandomBytes(n int) []byte {
	defer timetrack.TimeTrack(time.Now())
	b := make([]byte, n)
	_, _ = rand.Read(b)
	return b
}
