package listener

import (
	"bufio"
	"bytes"
	"crypto/tls"
	"errors"
	"fmt"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/common/log"
	"gitlab.com/T4cC0re/muzzleflash/util"
	timetrack "gitlab.com/T4cC0re/time-track"
	"io"
	"net"
	"net/http"
	"os"
	"sort"
	"strings"
	"sync"
	"time"
)

func init() {
	prometheus.MustRegister(traffic_r)
	prometheus.MustRegister(traffic_w)
	prometheus.MustRegister(traffic_opened_conns)
	prometheus.MustRegister(traffic_closed_conns)
	go collectTraffic()
	go listOpen()
}

func collectTraffic() {
	for {
		select {
		case r := <-increment_r:
			traffic_r.Add(r)
		case w := <-increment_w:
			traffic_w.Add(w)
		case o := <-increment_opened_conns:
			traffic_opened_conns.Add(o)
		case c := <-increment_closed_conns:
			traffic_closed_conns.Add(c)
		}
	}
}

var increment_r = make(chan float64, 1024)
var increment_w = make(chan float64, 1024)
var increment_opened_conns = make(chan float64, 1024)
var increment_closed_conns = make(chan float64, 1024)
var traffic_r = prometheus.NewCounter(prometheus.CounterOpts{Name: "muzzleflash_bytes_read", Help: "Total bytes read from clients (inbound)"})
var traffic_w = prometheus.NewCounter(prometheus.CounterOpts{Name: "muzzleflash_bytes_written", Help: "Total bytes written to clients (outbound)"})
var traffic_opened_conns = prometheus.NewCounter(prometheus.CounterOpts{Name: "muzzleflash_conns_opened", Help: "Connections to clients that were opened"})
var traffic_closed_conns = prometheus.NewCounter(prometheus.CounterOpts{Name: "muzzleflash_conns_closed", Help: "Connections to clients that were closed"})

type Listener struct {
	TlsConfig  *tls.Config
	TLS        *http.Server
	Plain      *http.Server
	AltSSHAddr string
}

type CertCallback func(*tls.ClientHelloInfo) (*tls.Certificate, error)

type Connections struct {
	mutex sync.RWMutex
	conns map[string]*Conn
}

var Conns = Connections{conns: map[string]*Conn{}}

func (c *Connections) add(conn *Conn) {
	defer timetrack.TimeTrack(time.Now())
	c.mutex.Lock()
	c.conns[conn.RemoteAddr().String()] = conn
	c.mutex.Unlock()
	increment_opened_conns <- 1
}

func (c *Connections) remove(conn *Conn) {
	defer timetrack.TimeTrack(time.Now())
	c.mutex.Lock()
	delete(c.conns, conn.RemoteAddr().String())
	c.mutex.Unlock()
	increment_closed_conns <- 1
}

func (c *Connections) Get(remote string) *Conn {
	defer timetrack.TimeTrack(time.Now())
	c.mutex.RLock()
	defer c.mutex.RUnlock()
	return c.conns[remote]
}

// TODO: Not spam this on CLI, but have some endpoint for it
func listOpen() {
	var buf []string
	for {
		Conns.mutex.RLock()
		buf = []string{}
		for _, conn := range Conns.conns {
			buf = append(buf, fmt.Sprintf("%s (%s)", conn.RemoteAddr().String(), conn.ConnectionId))
		}
		Conns.mutex.RUnlock()
		sort.Strings(buf)
		_, _ = fmt.Printf("--------- OPEN CONNECTIONS ---------\n%s\n-----------------------------------\n", strings.Join(buf, "\n"))
		time.Sleep(time.Second)
	}
}

func ConnStateCallback(c net.Conn, cs http.ConnState) {
	defer timetrack.TimeTrack(time.Now())

	conn := Conns.Get(c.RemoteAddr().String())
	if conn == nil {
		// Nothing to see here
		return
	}

	log.Warnf("Conn %s is now %s", conn.ConnectionId, cs)

	switch cs {
	case http.StateNew:
		if cc, ok := c.(*tls.Conn); ok {
			state := cc.ConnectionState()
			switch state.Version {
			case tls.VersionSSL30:
				log.Warnf("Conn %s: VersionSSL30", conn.ConnectionId)
			case tls.VersionTLS10:
				log.Warnf("Conn %s: VersionTLS10", conn.ConnectionId)
			case tls.VersionTLS11:
				log.Warnf("Conn %s: VersionTLS11", conn.ConnectionId)
			case tls.VersionTLS12:
				log.Warnf("Conn %s: VersionTLS12", conn.ConnectionId)
			case tls.VersionTLS13:
				log.Warnf("Conn %s: VersionTLS13", conn.ConnectionId)
			default:
				log.Warnf("Conn %s: unknown TLS version", conn.ConnectionId)
			}
		} else {
			log.Warnf("Conn %s: plain", conn.ConnectionId)
		}
	case http.StateIdle:
		fallthrough
	case http.StateHijacked:
		fallthrough
	case http.StateClosed:
		break
	}
}

func NewListener(handler http.Handler, callback CertCallback, AltSSHAddr string) *Listener {
	l := &Listener{AltSSHAddr: AltSSHAddr}
	_ = os.Setenv("GODEBUG", "tls13=1") // Needed for TLS1.3 opt-in until go 1.13
	l.TlsConfig = &tls.Config{
		SessionTicketsDisabled:   true, // SessionTickets compromise security!
		GetCertificate:           callback,
		MinVersion:               tls.VersionTLS12,
		CurvePreferences:         []tls.CurveID{tls.CurveP521, tls.CurveP384, tls.CurveP256},
		PreferServerCipherSuites: true,
		CipherSuites: []uint16{
			// TLS 1.3 ciphers
			tls.TLS_AES_128_GCM_SHA256,
			tls.TLS_AES_256_GCM_SHA384,
			tls.TLS_CHACHA20_POLY1305_SHA256,
			// TLS 1.2 ciphers
			tls.TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384,
			tls.TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256,
			tls.TLS_ECDHE_RSA_WITH_AES_256_CBC_SHA,
			// weak TLS 1.2 ciphers that are not required for most clients
			//tls.TLS_RSA_WITH_AES_256_GCM_SHA384,
			//tls.TLS_RSA_WITH_AES_128_GCM_SHA256,
			//tls.TLS_RSA_WITH_AES_256_CBC_SHA,
		},
	}
	l.Plain = &http.Server{
		Handler:           handler,
		TLSConfig:         nil,
		ReadHeaderTimeout: 5 * time.Second,
		// WriteTimeout:      10 * time.Second, // Should not be needed and can break things
		IdleTimeout:    185 * time.Second, // default TCP Keepalive + 5 seconds
		ConnState:      ConnStateCallback,
		MaxHeaderBytes: 1 << 20,
	}
	l.TLS = &http.Server{
		TLSConfig:         l.TlsConfig,
		Handler:           handler,
		ReadHeaderTimeout: 5 * time.Second,
		// WriteTimeout:      10 * time.Second, // Should not be needed and can break things
		IdleTimeout:    185 * time.Second, // default TCP Keepalive + 5 seconds
		ConnState:      ConnStateCallback,
		MaxHeaderBytes: 1 << 20,
	}

	return l
}

func (l *Listener) ListenPlain(address string) error {
	listener, err := net.Listen("tcp", address)
	if err != nil {
		return err
	}
	return l.Plain.Serve(customListener{(listener).(*net.TCPListener), l})
}

func (l *Listener) ListenTLS(address string) error {
	listener, err := net.Listen("tcp", address)
	if err != nil {
		return err
	}
	return l.TLS.ServeTLS(customListener{(listener).(*net.TCPListener), l}, "", "")
}

func (l *Listener) ServeSSH(src *Conn) {
	defer src.Close()

	src.ignoreTimeout = true
	_ = src.SetDeadline(time.Time{})

	dst, err := net.Dial("tcp", src.listener.AltSSHAddr)
	if err != nil {
		fmt.Printf("Dial failed '%s'\n", err)
		return
	}
	defer dst.Close()

	wg := sync.WaitGroup{}
	wg.Add(2)

	go func() {
		_, _ = io.Copy(dst, src)
		wg.Done()
	}()
	go func() {
		_, _ = io.Copy(src, dst)
		wg.Done()
	}()
	wg.Wait()
}

type customListener struct {
	*net.TCPListener
	listener *Listener
}

const BYTES_TO_READ = 4

type Conn struct {
	net.Conn
	ConnectionId   string
	buf            *bufio.ReadWriter
	listener       *Listener
	traffic_r      uint64
	traffic_w      uint64
	last_r         uint64
	last_w         uint64
	ignoreTimeout  bool
	openTime       time.Time
	initialRequest bool
}

var noerror = errors.New("no error - SSH mux")
var SSHMux = &SSHMuxErr{errors.New("no error - SSH mux")}

// An Error represents a network error.
type SSHMuxErr struct {
	error
}

func (_ SSHMuxErr) Temporary() bool {
	return true
}

func (_ SSHMuxErr) Timeout() bool {
	return false
}

func (ln customListener) Accept() (net.Conn, error) {
	tcpConn, err := ln.AcceptTCP()
	if err != nil {
		return nil, err
	}
	measureTime := time.Now()
	defer timetrack.TimeTrack(measureTime) // Track after accept, as that blocks
	read := bufio.NewReaderSize(tcpConn, BYTES_TO_READ)
	write := bufio.NewWriter(tcpConn)
	buf := bufio.NewReadWriter(read, write)
	b := Conn{
		Conn:           tcpConn,
		ConnectionId:   fmt.Sprintf("%x", util.GenerateRandomBytes(8)),
		buf:            buf,
		listener:       ln.listener,
		ignoreTimeout:  false,
		initialRequest: true,
		openTime:       measureTime,
	}
	Conns.add(&b)
	log.With("connectionId", b.ConnectionId).With("remote", tcpConn.RemoteAddr().String()).Warn("open")

	_ = tcpConn.SetReadDeadline(time.Now().Add(READ_TIMEOUT_TIME))
	_ = tcpConn.SetWriteDeadline(time.Now().Add(WRITE_TIMEOUT_TIME))

	if ln.listener.AltSSHAddr != "" {
		// Only peek if we have alt-ssh configured
		peek, _ := b.buf.Peek(BYTES_TO_READ)
		//fmt.Printf("magic: % 02x\n", peek)
		if bytes.Compare(peek, []byte("SSH-")) == 0 {
			//fmt.Println("IS SSH! muxing away :)")
			go ln.listener.ServeSSH(&b)
			// This will produce an ugly log with 'http: Accept error: accept: connection reset by peer; retrying in 5ms', but who cares?
			// Connection will also be closed by ServeSSH
			return nil, SSHMux
		}
	}
	return &b, nil
}

const (
	READ_TIMEOUT_BYTE = 1024 * 30
	READ_TIMEOUT_TIME = 30 * time.Second
	// = 1 KiB/s avg. over 30 sec.

	WRITE_TIMEOUT_BYTE = 1024 * 30
	WRITE_TIMEOUT_TIME = 30 * time.Second
	// = 1 KiB/s avg. over 30 sec.
)

func (c *Conn) Read(b []byte) (n int, err error) {
	n, err = c.buf.Read(b)
	increment_r <- float64(n)
	c.traffic_r += uint64(n)

	/*
			When we have read at least N byte before we timeout, then we extend the deadline
		    If ignoreTimeout is set, we assume someone else will handle this
	*/
	if c.traffic_r >= c.last_r+READ_TIMEOUT_BYTE && !c.ignoreTimeout {
		c.last_r = c.traffic_r
		_ = c.SetDeadline(time.Now().Add(READ_TIMEOUT_TIME))
	}

	return
}

func (c *Conn) Write(b []byte) (n int, err error) {
	n, err = c.buf.Write(b)
	_ = c.buf.Flush()
	increment_w <- float64(n)
	c.traffic_w += uint64(n)

	/*
			When we have written at least N byte before we timeout, then we extend the deadline
		    If ignoreTimeout is set, we assume someone else will handle this
	*/
	if c.traffic_w >= c.last_w+WRITE_TIMEOUT_BYTE && !c.ignoreTimeout {
		c.last_w = c.traffic_w
		_ = c.SetDeadline(time.Now().Add(WRITE_TIMEOUT_TIME))
	}

	return
}

func (c *Conn) Close() error {
	// Close our parent
	err := c.Conn.Close()
	log.With("connectionId", c.ConnectionId).With("remote", c.RemoteAddr().String()).Warn("close")
	Conns.remove(c)
	if err != nil {
		return err
	}
	return nil
}

func (c *Conn) MeasureTimeSinceOpened() {
	if !c.initialRequest {
		return
	}
	c.initialRequest = false
	timetrack.TimeTrackCustom(c.openTime, "TimeToHandle")
}
