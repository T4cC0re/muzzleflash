package types

import (
	"fmt"
	"gitlab.com/T4cC0re/muzzleflash/listener"
	timetrack "gitlab.com/T4cC0re/time-track"
	"net/http"
	"sync"
	"time"
)

type Request struct {
	IsTLS          bool
	Scheme         string
	VHost          *VHost
	Backend        *Backend
	Hostname       string
	Authenticated  bool
	Conn           *listener.Conn
	RequestId      string
	OrigRequest    *http.Request
	ResponseWriter *http.ResponseWriter
	HTTP2          bool
	HTTP2Pusher    http.Pusher
	Status         int
}

func (req *Request) SetHeader(header, value string) {
	defer timetrack.TimeTrack(time.Now())
	(*req.ResponseWriter).Header().Set(header, value)
}

func (req *Request) WriteHeader(code int) {
	defer timetrack.TimeTrack(time.Now())
	(*req.ResponseWriter).WriteHeader(code)
	req.Status = code
}

func (req *Request) Redirect(target string, code int) {
	defer timetrack.TimeTrack(time.Now())
	http.Redirect(*req.ResponseWriter, req.OrigRequest, target, code)
	req.Status = code
}

type DialogData struct {
	Error     bool
	ErrorText string
	Redirect  string
	Hostname  string
	Username  string
}

type KeyPair struct {
	Certificate string
	Keyfile     string
}

type VHost struct {
	Host            string        `json:"host,omitempty"`
	ForceTLS        bool          `json:"force_tls,omitempty"`
	PureLogin       bool          `json:"pure_login,omitempty"`
	ConfirmLogin    bool          `json:"confirm_login,omitempty"`
	LoginHost       string        `json:"login_host,omitempty"`
	Backends        []*Backend    `json:"backends,omitempty"`
	BackendRequests uint64        `json:"-,omit"`
	AccessMutex     *sync.RWMutex `json:"-,omit"`
	Protected       bool          `json:"protected,omitempty"`
	AllowMetrics    bool          `json:"allow_metrics,omitempty"`
	HSTS            *HSTS         `json:"hsts,omitempty"`
}

func (base *HSTS) Clone() *HSTS {
	return &HSTS{
		Enabled:           base.Enabled,
		Preload:           base.Preload,
		MaxAge:            base.MaxAge,
		IncludeSubdomains: base.IncludeSubdomains,
	}
}

func (hsts *HSTS) Header() (header string) {
	header = fmt.Sprintf("max-age=%d", hsts.MaxAge)
	if hsts.IncludeSubdomains {
		header += "; includeSubDomains"
	}
	if hsts.Preload {
		header += "; preload"
	}
	return
}

type HSTS struct {
	Enabled           bool   `json:"enabled"`
	Preload           bool   `json:"preload"`
	MaxAge            uint32 `json:"max_age"`
	IncludeSubdomains bool   `json:"include_subdomains"`
}

type Backend struct {
	IsTLS             bool         `json:"tls,omitempty"`
	IgnoreCertificate bool         `json:"ignore_certificate,omitempty"`
	Host              string       `json:"host,omitempty"`
	Port              uint16       `json:"port,omitempty"`
	HostHeader        string       `json:"host_header,omitempty"`
	SetForwardedProto bool         `json:"set_forwarded_proto,omitempty"`
	Client            *http.Client `json:"_,omit"`
}

// Grabs a round-robin backend for the given host name
func (v *VHost) GetBackend(blacklist ...uint64) (backend *Backend, id uint64) {
	defer timetrack.TimeTrack(time.Now())
	v.AccessMutex.RLock()
	defer v.AccessMutex.RUnlock()
	// Panics otherwise
	if l := len(v.Backends); l > 0 {
		if bl := len(blacklist); bl > 0 {
			if bl >= l {
				return nil, 0
			}
			for _, blackId := range blacklist {
				id = v.BackendRequests % uint64(l)
				v.BackendRequests++
				if blackId != id {
					break
				}
			}
		} else {
			id = v.BackendRequests % uint64(l)
			v.BackendRequests++
		}

		backend = v.Backends[id]
	}
	return
}
