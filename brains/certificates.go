package brains

import (
	"crypto/tls"
	"crypto/x509"
	"gitlab.com/T4cC0re/time-track"
	"log"
	"strings"
	"sync"
	"time"
)

type CertificateManager struct {
	// all certificates referenced anywhere MUST be accessible here. This slice is used to keep OCSP etc up to date
	certificates []*tls.Certificate
	// This certificate it used by FindCertificate if no certificate matches the ClientHello
	fallbackCertificate *tls.Certificate
	// caches SNI names to certificates
	cache map[string]*tls.Certificate
	// Allows to create cacheable SNIs from wildcards
	certMap map[string]*tls.Certificate
	// Locking for thread-safe access
	sync.RWMutex
}

func NewCertificateManager() *CertificateManager {
	c := &CertificateManager{}
	c.cache = map[string]*tls.Certificate{}
	c.certMap = map[string]*tls.Certificate{}
	return c
}

func (c *CertificateManager) AddBundle(bundle string) error {
	return c.AddKeyPair(bundle, bundle)
}

func (c *CertificateManager) AddKeyPair(certFile string, keyFile string) error {
	defer timetrack.TimeTrack(time.Now())

	if keyFile == "" {
		keyFile = certFile
	}
	cert, err := tls.LoadX509KeyPair(certFile, keyFile)
	if err != nil {
		return err
	}

	x509Cert, err := x509.ParseCertificate(cert.Certificate[0])
	if err != nil {
		return err
	}
	cert.Leaf = x509Cert
	log.Printf(
		"Loaded certificate '%s': CN=%s SAN=%s SIG=%s KEY=%s",
		certFile,
		x509Cert.Subject.CommonName,
		strings.Join(x509Cert.DNSNames, ","),
		x509Cert.SignatureAlgorithm.String(),
		x509Cert.PublicKeyAlgorithm.String(),
	)

	if len(x509Cert.Subject.CommonName) > 0 {
		err = c.addToCertificateMap(x509Cert.Subject.CommonName, &cert)
		if err != nil {
			return err
		}
	}
	for _, SAN := range x509Cert.DNSNames {
		err = c.addToCertificateMap(SAN, &cert)
		if err != nil {
			return err
		}
	}
	// Yes, there are actually certs issued to IPs...
	for _, IP := range x509Cert.IPAddresses {
		err = c.addToCertificateMap(IP.String(), &cert)
		if err != nil {
			return err
		}
	}

	return nil
}

func (c *CertificateManager) addToCertificateMap(name string, certificate *tls.Certificate) error {
	defer timetrack.TimeTrack(time.Now())

	// Remove trailing dot, which is permitted by spec, but messes with the matching logic
	name = strings.TrimRight(name, ".")

	c.Lock()
	defer c.Unlock()

	// Add non-wildcards to cache for performance
	if !strings.HasPrefix(name, "*.") {
		c.cache[name] = certificate
	}

	c.certMap[name] = certificate
	if c.fallbackCertificate == nil {
		c.fallbackCertificate = certificate

		log.Printf(
			"Set certificate as fallback: CN=%s SAN=%s SIG=%s KEY=%s",
			certificate.Leaf.Subject.CommonName,
			strings.Join(certificate.Leaf.DNSNames, ","),
			certificate.Leaf.SignatureAlgorithm.String(),
			certificate.Leaf.PublicKeyAlgorithm.String(),
		)
	}
	c.certificates = append(c.certificates, certificate)

	return nil
}

func (c *CertificateManager) FindCertificate(hello *tls.ClientHelloInfo) (cert *tls.Certificate, err error) {
	defer timetrack.TimeTrack(time.Now())

	var ok bool

	c.RLock()
	// Can't defer RUnlock, because of mixed read and read-write ops

	if cert, ok = c.cache[hello.ServerName]; ok {
		c.RUnlock()
		return
	}
	if len(hello.ServerName) == 0 {
		log.Print("Encountered empty ClientHello SNI")
		cert = c.fallbackCertificate
		c.RUnlock()
		return
	}
	log.Printf("Encountered uncached ClientHello SNI '%s'", hello.ServerName)

	var wildcard string
	for wildcard, cert = range c.certMap {
		if nameMatchesWildcard(hello.ServerName, wildcard) {
			// Swap read lock for read-write lock
			c.RUnlock()
			c.Lock()
			// Write cache entry
			c.cache[hello.ServerName] = cert
			log.Printf("Selected cert %s (S/N %s) for SNI %s", cert.Leaf.Subject.CommonName, cert.Leaf.SerialNumber, hello.ServerName)
			// free lock and return
			c.Unlock()
			return
		}
	}

	cert = c.fallbackCertificate
	if cert == nil {
		// No cert was given
		log.Printf("No fallback certificate available for SNI '%s'", hello.ServerName)
		// free lock and return
		c.RUnlock()
		return
	}

	// Also cache fallbacks
	// Swap read lock for read-write lock
	c.RUnlock()
	c.Lock()
	c.cache[hello.ServerName] = cert
	log.Printf("Selected fallback cert %s (S/N %s) for SNI %s", cert.Leaf.Subject.CommonName, cert.Leaf.SerialNumber, hello.ServerName)
	// free lock and return
	c.Unlock()
	return
}

func nameMatchesWildcard(name string, wildcard string) bool {
	defer timetrack.TimeTrack(time.Now())

	// Remove trailing dot, which is permitted by spec, but messes with the matching logic
	name = strings.TrimRight(name, ".")
	wildcard = strings.TrimRight(wildcard, ".")

	if strings.EqualFold(name, wildcard) {
		return true
	}

	segmentsWildcard := strings.Split(wildcard, ".")
	segmentsName := strings.Split(name, ".")
	if len(segmentsName) != len(segmentsWildcard) {
		return false
	}
	// If everything after the first dot is the same
	if strings.EqualFold(strings.Join(segmentsWildcard[1:], "."), strings.Join(segmentsName[1:], ".")) {
		if segmentsWildcard[0] == "*" || strings.EqualFold(segmentsWildcard[0], segmentsName[0]) {
			return true
		}
	}

	return false
}
