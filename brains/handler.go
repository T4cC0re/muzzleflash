package brains

import (
	"crypto/tls"
	"fmt"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"github.com/prometheus/common/log"
	"gitlab.com/T4cC0re/muzzleflash/authentication"
	"gitlab.com/T4cC0re/muzzleflash/listener"
	"gitlab.com/T4cC0re/muzzleflash/types"
	"gitlab.com/T4cC0re/muzzleflash/ui"
	"gitlab.com/T4cC0re/muzzleflash/util"
	"gitlab.com/T4cC0re/time-track"
	"io"
	"net"
	"net/http"
	"net/url"
	"strings"
	"sync"
	"time"
)

const METRICS_PATH = "/-/muzzleflash/metrics"

type Handler struct {
	mapMutex *sync.RWMutex
	vHosts   map[string]*types.VHost
}

var (
	uiTemplate *ui.Template
	prometheus = promhttp.Handler()
)

func NewHandler() *Handler {
	h := &Handler{}
	h.mapMutex = &sync.RWMutex{}
	h.vHosts = map[string]*types.VHost{}

	return h
}

func ParseTemplates(basedir string) {
	var err error
	uiTemplate, err = ui.NewTemplate(basedir)
	if err != nil {
		log.Fatal(err)
	}
}

func setupClient(backend *types.Backend) {
	defer timetrack.TimeTrack(time.Now())

	if backend == nil || backend.Client != nil {
		return
	}

	defaultTransport := http.DefaultTransport.(*http.Transport)

	transport := &http.Transport{
		Proxy:                 defaultTransport.Proxy,
		DialContext:           defaultTransport.DialContext,
		MaxIdleConns:          defaultTransport.MaxIdleConns,
		IdleConnTimeout:       defaultTransport.IdleConnTimeout,
		ExpectContinueTimeout: defaultTransport.ExpectContinueTimeout,
		TLSHandshakeTimeout:   defaultTransport.TLSHandshakeTimeout,
	}
	// Create new Transport that ignores self-signed SSL
	transport.TLSClientConfig = &tls.Config{
		InsecureSkipVerify: backend.IgnoreCertificate,
	}
	backend.Client = &http.Client{
		Transport:     transport,
		CheckRedirect: noRedirect,
	}
}

func noRedirect(_ *http.Request, _ []*http.Request) error {
	return http.ErrUseLastResponse
}

func (h *Handler) AddBackend(vHost string, backend *types.Backend) {
	defer timetrack.TimeTrack(time.Now())

	setupClient(backend)

	h.mapMutex.RLock()
	defer h.mapMutex.RUnlock()
	h.vHosts[vHost].AccessMutex.Lock()
	defer h.mapMutex.Unlock()
	h.vHosts[vHost].Backends = append(h.vHosts[vHost].Backends, backend)
	log.Infof("Added backend to vHost '%s': %+v", vHost, backend)
}

func (h *Handler) AddVHost(vHost *types.VHost) {
	defer timetrack.TimeTrack(time.Now())

	vHost.ForceTLS = vHost.Protected || vHost.ForceTLS || vHost.PureLogin
	if vHost.AccessMutex == nil {
		vHost.AccessMutex = &sync.RWMutex{}
	}
	if vHost.LoginHost == "" {
		vHost.LoginHost = vHost.Host
	}
	h.mapMutex.Lock()
	h.vHosts[vHost.Host] = vHost
	h.mapMutex.Unlock()
	vHost.AccessMutex.Lock()
	defer vHost.AccessMutex.Unlock()
	log.Infof("Added vHost '%s'", vHost.Host)
	for _, backend := range vHost.Backends {
		setupClient(backend)

		log.Infof("Added backend to vHost '%s': %+v", vHost.Host, backend)
	}
}

func elevate(request *types.Request) {
	defer timetrack.TimeTrack(time.Now())

	logReq(request).Info("Elevating")
	request.Redirect(fmt.Sprintf("https://%s%s", request.Hostname, request.OrigRequest.URL.RequestURI()), http.StatusMovedPermanently)
}

func hostname(name string) (hostname string) {
	hostname, _, _ = net.SplitHostPort(name)
	if hostname == "" {
		hostname = name
	}
	return
}

func (h *Handler) ServeHTTP(w http.ResponseWriter, origRequest *http.Request) {
	defer origRequest.Body.Close()

	req := &types.Request{
		OrigRequest:    origRequest,
		RequestId:      fmt.Sprintf("%x", util.GenerateRandomBytes(8)),
		Conn:           listener.Conns.Get(origRequest.RemoteAddr),
		ResponseWriter: &w,
	}
	defer logAccess(req)
	if req.Conn == nil {
		// If this block gets executed something is fishy
		internalServerError(req)
		return
	}
	req.Conn.MeasureTimeSinceOpened()

	req.HTTP2Pusher, req.HTTP2 = w.(http.Pusher)
	req.Hostname = hostname(origRequest.Host)
	req.IsTLS = origRequest.TLS != nil
	if req.IsTLS && origRequest.TLS.ServerName != "" {
		if req.HTTP2 && !strings.EqualFold(origRequest.TLS.ServerName, req.Hostname) && req.Hostname != "" {
			// Send 421 to indicate to HTTP/2 client, that it should create a new connection for a request on a new domain.
			// See https://bugs.chromium.org/p/chromium/issues/detail?id=937682
			logReq(req).With("SNI", origRequest.TLS.ServerName).With("Header", req.Hostname).Warn("mismatched SNI")
			writeObligatoryHeaders(req)
			req.WriteHeader(421)
			return
		}
		req.Hostname = hostname(origRequest.TLS.ServerName)
		req.Scheme = "https"
	} else if req.IsTLS {
		req.Scheme = "https"
	} else {
		req.Scheme = "http"
	}

	vHost := h.vHostByHostname(req.Hostname)

	if vHost == nil {
		logReq(req).Infof("Could not get vHost for '%s'", req.Hostname)
		badGateway(req)
		return
	}

	req.VHost = vHost

	logReq(req).Infof("Selected vHost for '%s'", req.Hostname)
	writeObligatoryHeaders(req)

	//TODO: Handle /-/muzzleflash/ paths muxed

	if vHost.AllowMetrics && strings.HasPrefix(origRequest.URL.Path, METRICS_PATH) {
		req.SetHeader("Mzfl-Upstream", "metrics")
		prometheus.ServeHTTP(w, origRequest)
		return
	}

	// Pass LOGIN_PATH to the authentication module
	if strings.HasPrefix(origRequest.URL.Path, authentication.LOGIN_PATH) || vHost.PureLogin {
		switch {
		// HandleFrontend always has to be TLS!
		case req.IsTLS == false:
			elevate(req)
		case req.VHost.LoginHost == "":
			fallthrough
		case req.VHost.LoginHost == req.VHost.Host:
			fallthrough
		case vHost.PureLogin:
			authentication.HandleFrontend(req)
		default:
			redirectToLoginPage(req)
		}
		return
	}

	if (vHost.Protected || vHost.ForceTLS) && !req.IsTLS {
		elevate(req)
		return
	}

	if vHost.Protected {
		var redirected bool
		req.Authenticated, redirected = authentication.IsAuthenticated(req)
		if redirected {
			return
		} else if !req.Authenticated {
			redirectToLoginPage(req)
			return
		}
	}

	var blacklist []uint64

selectBackend:
	for {
		backend, backendId := vHost.GetBackend(blacklist...)
		if backend == nil {
			break selectBackend
		}
		blacklist = append(blacklist, backendId) // In case we fail

		if backend == nil || backend.Client == nil {
			continue selectBackend
		}

		req.Backend = backend

		if backend.HostHeader != "" {
			origRequest.Host = backend.HostHeader
		}

		origRequest.URL.Host = fmt.Sprintf("%s:%d", backend.Host, backend.Port)
		origRequest.RequestURI = "" // Has to be empty in client requests
		if backend.IsTLS {
			origRequest.URL.Scheme = "https"
		} else {
			origRequest.URL.Scheme = "http"
		}
		if backend.SetForwardedProto {
			origRequest.Header.Set("x-forwarded-proto", req.Scheme)
		}

		upstream := fmt.Sprintf("%s://%s:%d", origRequest.URL.Scheme, backend.Host, backend.Port)
		logReq(req).Infof("inbound request on host '%s' brokered to upstream '%s'", req.Hostname, upstream)

		// Perform request to upstream
		resp, err := backend.Client.Do(origRequest)
		if err != nil {
			logReq(req).Error(err)
			e := err.(*url.Error)
			if e.Timeout() {
				gatewayTimeout(req)
			} else {
				continue selectBackend
			}
			return
		}

		// Reflect headers to downstream
		for header, values := range resp.Header {
			for _, value := range values {
				w.Header().Add(header, value)
			}
		}

		// End the request
		writeObligatoryHeaders(req)
		req.WriteHeader(resp.StatusCode)

		// Accelerate copy by allocating a 1MiB buffer (instead of default 32KiB in Go1.11)
		buf := make([]byte, 1024*1024)
		if _, err := io.CopyBuffer(w, resp.Body, buf); err != nil {
			// Too late to change the status code, but we can log it
			logReq(req).Errorf("error while copying buffer %s", err)
		}
		if err := resp.Body.Close(); err != nil {
			// Too late to change the status code, but we can log it
			logReq(req).Error("error while closing response body %s", err)
		}
		return
	}
	serviceUnavailable(req)
}

func logAccess(request *types.Request) {
	defer timetrack.TimeTrack(time.Now())
	backendHost := "internal"
	if request.Backend != nil {
		backendHost = request.Backend.Host
	}
	logReq(request).
		With("status", request.Status).
		With("URL", request.OrigRequest.URL.Path).
		With("backend", backendHost).
		Info("access")
}

func logReq(request *types.Request) log.Logger {
	defer timetrack.TimeTrack(time.Now())
	if request == nil {
		return log.Base()
	}
	var vhost string
	if request.VHost != nil {
		vhost = request.VHost.Host
	}
	return log.
		With("vHost", vhost).
		With("authenticated", request.Authenticated).
		With("tls", request.IsTLS).
		With("http2", request.HTTP2).
		With("scheme", request.Scheme).
		With("connectionId", request.Conn.ConnectionId).
		With("requestId", request.RequestId)
}

func gatewayTimeout(request *types.Request) {
	renderError(request, 504)
	return
}

func internalServerError(request *types.Request) {
	renderError(request, 500)
	return
}

func serviceUnavailable(request *types.Request) {
	renderError(request, 503)
	return
}

func badGateway(request *types.Request) {
	renderError(request, 502)
	return
}

func renderError(request *types.Request, code int) {
	defer timetrack.TimeTrack(time.Now())
	writeObligatoryHeaders(request)
	request.WriteHeader(code)
	if isHTMLRequest(request.OrigRequest) {
		_ = uiTemplate.Error(code, *request.ResponseWriter)
	}
	logAccess(request)
}

func isHTMLRequest(origRequest *http.Request) bool {
	defer timetrack.TimeTrack(time.Now())

	acceptedTypes := strings.Split(origRequest.Header.Get("accept"), ",")
	for _, v := range acceptedTypes {
		if strings.Contains(v, "html") {
			return true
		}
	}
	return false
}

func redirectToLoginPage(request *types.Request) {
	defer timetrack.TimeTrack(time.Now())

	logReq(request).Info("Redirecting to login page")
	writeObligatoryHeaders(request)
	request.Redirect(authentication.BuildLink(request.VHost.LoginHost, request.VHost.ConfirmLogin, request.OrigRequest), http.StatusFound)
	return
}

func (h *Handler) vHostByHostname(hostname string) *types.VHost {
	defer timetrack.TimeTrack(time.Now())
	h.mapMutex.RLock()

	if vhost := h.vHosts[hostname]; vhost != nil {
		h.mapMutex.RUnlock()
		return vhost
	}

	for k, v := range h.vHosts {
		if nameMatchesWildcard(hostname, k) {
			h.mapMutex.RUnlock()
			h.mapMutex.Lock()
			h.vHosts[hostname] = v
			h.mapMutex.Unlock()
			return v
		}
	}
	h.mapMutex.RUnlock()

	return nil
}

func writeObligatoryHeaders(request *types.Request) {
	defer timetrack.TimeTrack(time.Now())
	request.SetHeader("server", "muzzleflash>>>")
	request.SetHeader("connection", "keep-alive")
	request.SetHeader("mzfl-request-id", request.RequestId)
	request.SetHeader("mzfl-conn-id", request.Conn.ConnectionId)
	if request.Backend == nil {
		request.SetHeader("Mzfl-Upstream", "internal")
	} else {
		request.SetHeader("Mzfl-Upstream", request.Backend.Host)
	}
	if request.VHost != nil && request.VHost.HSTS.Enabled && request.IsTLS {
		request.SetHeader("Strict-Transport-Security", request.VHost.HSTS.Header())
	}

}
