package brains

import (
	"github.com/ghodss/yaml"
	"github.com/prometheus/common/log"
	"gitlab.com/T4cC0re/muzzleflash/authentication"
	"gitlab.com/T4cC0re/muzzleflash/types"
	"gitlab.com/T4cC0re/time-track"
	"io/ioutil"
	"os"
	"time"
)

type Certificate struct {
	KeyFile     string `json:"key,omitempty"`
	Certificate string `json:"cert,omitempty"`
	Bundle      string `json:"bundle,omitempty"`
}

type PlainCredential struct {
	Username string `json:"username,omitempty"`
	Password string `json:"password,omitempty"`
}

type Credentials struct {
	Plain []PlainCredential `json:"plain,omitempty"`
}
type Config struct {
	Cerfificates  []Certificate           `json:"certs,omitempty"`
	Credentials   Credentials             `json:"credentials,omitempty"`
	VHosts        map[string]*types.VHost `json:"hosts,omitempty"`
	JWTSecret     string                  `json:"jwt_secret,omitempty"`
	ListenerPlain string                  `json:"listener_plain,omitempty"`
	ListenerTLS   string                  `json:"listener_tls,omitempty"`
	AssetDir      string                  `json:"asset_dir,omitempty"`
	AltSSHAddr    string                  `json:"alt_ssh_addr,omitempty"`
	HSTS          types.HSTS              `json:"hsts,omitempty"`
}

func LoadConfig(configFile string, handler *Handler, manager *CertificateManager) (cfg *Config, err error) {
	defer timetrack.TimeTrack(time.Now())

	file, _ := os.Open(configFile)
	data, _ := ioutil.ReadAll(file)

	var config Config
	if err := yaml.Unmarshal(data, &config); err != nil {
		return nil, err
	}

	if config.JWTSecret != "" {
		authentication.SetJWTSecret([]byte(config.JWTSecret))
	}

	for _, cert := range config.Cerfificates {
		if cert.Bundle != "" {
			err = manager.AddBundle(cert.Bundle)
		} else {
			err = manager.AddKeyPair(cert.Certificate, cert.KeyFile)
		}
		if err != nil {
			return
		}
	}

	for _, plainCred := range config.Credentials.Plain {
		if plainCred.Username == "" || plainCred.Password == "" {
			continue
		}

		authentication.AddPlainCredential(plainCred.Username, plainCred.Password)
	}

	for name, host := range config.VHosts {
		host.Host = name
		if host.HSTS == nil {
			host.HSTS = config.HSTS.Clone()
		}
		if host.HSTS.MaxAge <= 0 {
			host.HSTS.Enabled = false // This will disable HSTS, as no valid Header could be rendered
		}
		log.Info(host.HSTS)
		handler.AddVHost(host)
	}

	authentication.ParseTemplates(config.AssetDir)
	ParseTemplates(config.AssetDir)

	if config.ListenerTLS == "" {
		config.ListenerTLS = ":443"
	}
	if config.ListenerPlain == "" {
		config.ListenerPlain = ":80"
	}

	cfg = &config

	return
}
