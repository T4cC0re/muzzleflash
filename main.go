package main

import (
	"github.com/sirupsen/logrus"
	"gitlab.com/T4cC0re/muzzleflash/brains"
	"gitlab.com/T4cC0re/muzzleflash/listener"
)

func main() {
	h := brains.NewHandler()
	c := brains.NewCertificateManager()
	conf, err := brains.LoadConfig("./config.yml", h, c)
	if err != nil {
		logrus.WithError(err).Fatal("could not parse config")
	}
	l := listener.NewListener(h, c.FindCertificate, conf.AltSSHAddr)
	go func() {
		if err := l.ListenPlain(conf.ListenerPlain); err != nil {
			logrus.WithError(err).Fatal("could not listen (plain)")
		}
	}()
	if err := l.ListenTLS(conf.ListenerTLS); err != nil {
		logrus.WithError(err).Fatal("could not listen (tls)")
	}
}
